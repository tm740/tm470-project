var hostname='tm470.jevo.co.uk';

function getMapData(pos) {
	console.log("getMapData called");
	$.get("https://"+hostname+":8443/api/get/nearme?lat="+pos.lat+"&lng="+pos.lng)
    	.done(function( data ) {
    		console.log( "Data Loaded: " + data );
		var jsonData = JSON.parse(data);
		for (var i = 0; i < jsonData.length; i++) {
    			name = jsonData[i].name;
    			notes = jsonData[i].notes;
    			lat = jsonData[i].geo.y;
    			lng = jsonData[i].geo.x;
				spaces = jsonData[i].spaces;
				cost = jsonData[i].cost;
				capacity = jsonData[i].capacity;

		addMapMarker();
		}
   
	});
}
	
	var activeInfoWindow;
	function addMapMarker() {
		var icon_default = 'res/32_parking_icon_blue.png';
		var icon_red = 'res/32_parking_icon_red.png';
		var icon_amber = 'res/32_parking_icon_amber.png';
		var icon_yellow = 'res/32_parking_icon_yellow.png';
		var icon_green = 'res/32_parking_icon_green.png';
		var icon_to_use = icon_default;
		
		if (typeof capacity !== "undefined") {
			if (capacity >= 90) {
				icon_to_use = icon_red;
			}
			else if (capacity >= 70 && capacity <=89) {
				icon_to_use = icon_amber;
			}
			else if (capacity >= 50 && capacity <=69) {
				icon_to_use = icon_yellow;
			}
			else if (capacity <= 49) {
				icon_to_use = icon_green;
			}
		}
		
		
		var icon = {
    			url: icon_to_use,
    			scaledSize: new google.maps.Size(20, 20)
    		}
		var marker = new google.maps.Marker({
    		position: {lat: parseFloat(lat), lng: parseFloat(lng)},
    		map: map,
    		title: name,
		icon: icon
  		});

		contentString = '<h2>'+name+'</h2>';
		contentString += '<a href="https://maps.google.com/?q='+lat+','+lng+'"><b>Directions</b></a>';
		contentString += '<br><b>Capacity:</b> ';
		if (spaces === 0) {
			contentString += 'Not available';
		} else {
			contentString += spaces;
		}
		contentString += '<br>';
		contentString += '<b>Spaces in use</b>: ' + capacity + '%<br>';
		contentString += '<br><u><b>Cost</b></u>'+'<br>';

		if (jQuery.isEmptyObject(cost) === false) {
			Object.keys(cost).forEach(function(key) {
				costpounds = Math.floor(cost[key]/100)
				costpence = cost[key]%100
				if (costpence.toString().length === 1){
					costpence = costpence.toString() + '0'
				}
				contentString += key + ' - £' + costpounds+'.'+costpence+'<br>';
			})
		}
		else {
			contentString += 'FREE<br>'
		}

		contentString += '<br><b>Notes</b>'+'<br>';
		if (notes) {	
			contentString += notes
		}
		else {
			contentString += 'No information available.'
		}
		

		var infowindow = new google.maps.InfoWindow({
        		content: contentString,
			maxWidth: 200
        	});
        	marker.addListener('click', function() {
			if (activeInfoWindow) { activeInfoWindow.close();}
        		infowindow.open(map, marker);
        		activeInfoWindow = infowindow;
        	}); 
	}

    



